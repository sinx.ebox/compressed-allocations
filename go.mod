module compressed-allocations

go 1.16

require (
	github.com/holiman/uint256 v1.2.1
	github.com/machinebox/graphql v0.2.2
	github.com/pkg/errors v0.9.1
	github.com/shopspring/decimal v1.3.1
)

require (
	github.com/avast/retry-go v3.0.0+incompatible
	github.com/ethereum/go-ethereum v1.10.25
	github.com/gammazero/workerpool v1.1.3
	github.com/matryer/is v1.4.0 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/rivo/uniseg v0.4.2 // indirect
	github.com/schollz/progressbar/v3 v3.11.0
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.13.0
	github.com/stretchr/testify v1.8.0
	golang.org/x/sys v0.0.0-20221010170243-090e33056c14 // indirect
	golang.org/x/term v0.0.0-20220919170432-7a66f970e087 // indirect
)
