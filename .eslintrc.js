module.exports = {
  root: true,
  env: {
    browser: false,
    es2021: true,
    node: true,
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2021,
    project: ['./tsconfig.json'],
  },
  plugins: ['@typescript-eslint'],
  extends: [
    'eslint:recommended',
    'airbnb-base',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'plugin:import/recommended',
    'plugin:import/typescript',
  ],
  settings: {
    'import/resolver': {
      'typescript': {},
    },
  },
  rules: {
    // Changed

    'array-element-newline': ['error', 'consistent'],
    'import/extensions': ['error', 'never'],
    'import/order': ['error', { 'newlines-between': 'always' }],
    'max-len': ['error', { code: 120, ignoreStrings: true }],
    'object-curly-newline': ['error', { consistent: true }],
    'object-curly-spacing': ['error', 'always'],
    'quote-props': ['error', 'consistent'],
    'quotes': ['error', 'single'],

    '@typescript-eslint/no-unused-vars': ['error', {
      args: 'all',
      argsIgnorePattern: '^_',
      vars: 'all',
      varsIgnorePattern: '^_',
    }],

    // Disabled

    'import/prefer-default-export': 'off',
    'no-console': 'off',
    'no-continue': 'off',
    'no-mixed-operators': 'off',
    'no-nested-ternary': 'off',
    'no-plusplus': 'off',
    'no-restricted-syntax': 'off',
    'no-use-before-define': 'off',
    'operator-linebreak': 'off',
    'prefer-template': 'off',
    'space-infix-ops': 'off',
    'no-bitwise': 'off',

    // Override: @typescript-eslint/X > eslint/X

    'lines-between-class-members': 'off',
    '@typescript-eslint/lines-between-class-members': 'error',

    'no-shadow': 'off',
    '@typescript-eslint/no-shadow': 'error',

    'no-useless-constructor': 'off',
    '@typescript-eslint/no-useless-constructor': 'error',

    'semi': 'off',
    '@typescript-eslint/semi': ['error'],
    '@typescript-eslint/member-delimiter-style': 'error',
  },
};
