import { Address } from './types';

export type AuditEntry = {
  date: string;
  type: 'credit' | 'flat_bonus' | 'volume_bonus';
  value: bigint;
  points: bigint;
};

export function formatAuditDate(d: Date): string {
  // 1. ####-##-##T##:##:##.###Z
  // 2. ####-##-##T##:##:##
  // 3. ####-##-## ##:##:##

  return d.toISOString()
    .slice(0, 19)
    .replace('T', ' ');
}

export function appendAudit(m: Map<Address, AuditEntry[]>, addr: Address, audit: AuditEntry) {
  const entries = m.get(addr);
  if (entries) {
    entries.push(audit);
  } else {
    m.set(addr, [audit]);
  }
}

export function mapAppend<T>(m: Map<string, T[]>, addr: string, item: T) {
  const entries = m.get(addr);
  if (entries) {
    entries.push(item);
  } else {
    m.set(addr, [item]);
  }
}
