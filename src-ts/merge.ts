import * as Papa from 'papaparse'
import * as fs from 'fs'
import * as paths from './paths'
import * as ethers from 'ethers'
import _ from 'lodash'
import { Point } from 'types'

const pathToPulsechain = paths.fromProject('pulsechain')
const pathToEth2Stakers = paths.fromProject('eth2-stakers')
const pathToDataRoot = paths.fromProject('.')

main().catch(console.error)

type CsvRow = [string, string, string]

type FullPoint = Point & {
  address: string;
}

async function main() {
  await exportCredits()
  await exportPointEvent()
  await exportRawReduced()
}

async function exportCredits() {
  const pulsechainFile = await fs.promises.readFile(pathToPulsechain.CREDITS_CSV_PATH)
  const {
    data: pulsechainCredits,
  } = Papa.parse<CsvRow>(pulsechainFile.toString(), {
    header: false,
  })
  const eth2StakersFile = await fs.promises.readFile(pathToEth2Stakers.CREDITS_CSV_PATH)
  const {
    data: eth2StakersCredits,
  } = Papa.parse<CsvRow>(eth2StakersFile.toString(), {
    header: false,
  })
  const addedCredits = _(eth2StakersCredits)
    .concat(pulsechainCredits)
    .filter(([address]) => !!address)
    .reduce((totals, [address, hexAmount, amount]) => {
      const account = ethers.getAddress(address)
      const rowAmount = ethers.toBigInt(amount)
      const existingAmount = totals.get(account) || 0n
      totals.set(account, rowAmount + existingAmount)
      return totals
    }, new Map<string, bigint>())
  const keys = [...addedCredits.keys()].sort()
  const ordered = keys.map((key) => ([
    key,
    `0x${(addedCredits.get(key) as bigint).toString(16)}`,
    addedCredits.get(key),
  ]))
  await writeCsv(pathToDataRoot.CREDITS_CSV_PATH, ordered)
}

async function exportPointEvent() {
  const pulsechainFile = await fs.promises.readFile(pathToPulsechain.POINT_EVENT_PATH)
  const {
    data: pulsechainPointEvents,
  } = Papa.parse<FullPoint>(pulsechainFile.toString(), {
    header: true,
  })

  const eth2StakersFile = await fs.promises.readFile(pathToEth2Stakers.CREDITS_CSV_PATH)
  const {
    data: eth2StakersCredits,
  } = Papa.parse<CsvRow>(eth2StakersFile.toString().trim(), {
    header: false,
  })
  const currency = `eth-${ethers.ZeroAddress}`
  const allPointEvents = pulsechainPointEvents.concat(
    eth2StakersCredits.map(([address, _hex, value]) => ({
      address,
      network: 'ethereum',
      hash: '',
      currency,
      type: 'staker',
      value: BigInt(value),
    })),
  )
  await writeCsv(pathToDataRoot.POINT_EVENT_PATH, allPointEvents)
}

async function exportRawReduced() {
  await fs.promises.copyFile(pathToPulsechain.RAW_REDUCED_CSV_PATH, pathToDataRoot.RAW_REDUCED_CSV_PATH)
}

async function writeCsv(filePath: string, rows: any[]) {
  const csv = Papa.unparse(rows, {
    header: false,
  })
  console.log(filePath, rows.length)
  await fs.promises.writeFile(filePath, csv)
}
