import * as fs from 'fs';

import { NetworkRuleMap, ruleForNetworks } from './rules';
import * as Rules from './rules';
import {
  PropertiesAsStrings,
  SacrificeFields,
  Sacrifice,
  Networks,
  Sources,
  SourcesList,
  SourceRankMap,
  rankForSources,
  Address,
} from './types';
import { scaleToDecimals18 } from './utils';
import {
  validateHeader,
  validateFieldCount,
  validateFieldType,
  validateField,
  validateRemap,
  InvalidRowError,
} from './validation';

type SacrificeRow = PropertiesAsStrings<SacrificeFields>;

const TX_HASH_RULE_MAP: NetworkRuleMap = {
  'sens': Rules.TXHASH_SENS,

  ...ruleForNetworks(Rules.HASH_256_0X_LC, [
    'arb',
    'avalanche',
    'binancesmartchain',
    'cronos',
    'ethereum',
    'ethereumclassic',
    'ethereum-optimism',
    'ftm',
    'fuse',
    'polygon',
    'theta',
    'zksync',
  ]),

  ...ruleForNetworks(Rules.HASH_256_LC, [
    'bitcoin',
    'bitcoin-cash',
    'cardano',
    'dogecoin',
    'eos',
    'litecoin',
    'monero',
    'stellar',
    'tron',
    'zcash',
  ]),

  ...ruleForNetworks(Rules.HASH_256_UC, [
    'terra',
    'binancechain',
    'ripple',
  ]),
};

const SOURCE_RANK_MAP: SourceRankMap = {
  ...rankForSources(0, SourcesList, 'sales/'),
  ...rankForSources(1, SourcesList, 'dexs/'),
  ...rankForSources(2, SourcesList, 'ccxt/'),
  ...rankForSources(3, SourcesList, 'dollarpegged'),
  ...rankForSources(4, SourcesList, 'coingecko'),
  ...rankForSources(5, SourcesList, 'missing'),
};

// Special exceptions of credit addresses that need to be re-mapped
const ADDR_REMAP = validateRemap(Rules.HASH_160_0X_LC, Rules.HASH_160_0X_LC, {
  '0xf2b1f3b06dcf2778707c257cfe387af3a587272b': '0xfb6d55200b1b113db222acd59a281ba5a4112530',
  '0x97e542ec6b81dea28f212775ce8ac436ab77a7df': '0xc874bb0381936d61c2da141c221f88ac921a99c1',
});

const TX_ADDR_REMAP = validateRemap(Rules.HASH_256_0X_LC, Rules.HASH_160_0X_LC, {
  '0x1810f826aca7dbc90ba0250065d200474136f4936f9af464aa70747047f3e3e2': '0xb4f4c572555e0d1bce6554406fafc46300f14420',
  '0x4260e45e8c9467322fc5e9c2452558d88109633a982e6e261190fa3614d741fa': '0x0bf8022da3ba8c6fb44ec36bd3e89a7ed5096ec5',
  '0x528a003114d11b3c47eababc852cf666204dbbddb0b23337b8c7eabb45bfc871': '0x0ff736e2c21cf6793564b8256bef8c20c182092c',
  '0xe099b109d3c94266d000c7b8a65a0386b290893a2f8254c424d035a8db65de2c': '0xb312dbdc15842c4c43e77bae204e4768ecf6c74c',
  '0xef089f7d67a0291f21a2dc85fcf3ae837bb61deb12860c6fde3595fd20a6cd07': '0xaeed7a23a89e439a19454b397467324d218181fa',
  '0xff8901b939fb8b44a729ffdc30179c3dfa63ed451efbf7f39a11521707c3641c': '0x31063d356a3f41c83bbc27eb49f01f6bc05ee149',
  '0x5984b9490177344aef8831f4ebeafa0c81b4c4bee003bedeb91d6a89971bdbdd': '0xd6f5dc0544c0963c541817896d0bbf510b34a8cd',
  '0xe79ba09e1a42467e207f9a025532b071b2193cf659ba0331784b9e8b0b61f91a': '0xbb485d577052269caac0904e680ae0ae71b9a954',
  '0x5f6d9c5e88a95b2089d2b2edf920f319916b853101d2c42a07e38cf15cd3edb8': '0x8b2614b7e819d1d3cf8193e004f40ab66702517a',
  '0x1b03b35805d3603622b88ffd6124b2a4820a1cc4b38a6491a847bf9253adc3c9': '0x8b2614b7e819d1d3cf8193e004f40ab66702517a',
  '0xde0e4e54df9dcf4887f9586c1f9423696f7ccd1a8e52cf9de925dd7859dbcc60': '0x8b2614b7e819d1d3cf8193e004f40ab66702517a',
  '0x6b2004d8aec5d40f76ddecfc9e700380098d1e4e7e5ef14f6b9526ddd02951d6': '0x8b2614b7e819d1d3cf8193e004f40ab66702517a',
  '0x1c038271c42dfabdfac130dcd72d34e4c2e5cfdf0e64578cc5817fc0b9ddbfdf': '0x8b2614b7e819d1d3cf8193e004f40ab66702517a',
  '0xd4f82a902ba9bb2a89c80e49e875377b0a968293673fa246b281222882dfb108': '0x8b2614b7e819d1d3cf8193e004f40ab66702517a',
  '0x7896be1c7e0704ad574444f57d4c52c3ec4060520719b549495c0de2c27d046e': '0x8b2614b7e819d1d3cf8193e004f40ab66702517a',
  '0x15098ffa0342aec25ac3e3941f5008899ed3d860a10537872be7e35707f2a7c6': '0xc15eed0fd21d0d1f9ed6c99036fd22b8330cd72d',
  '0x13d5dec6e1afac735358b68ab5e0b868c042047d6752b44ee2a1f4be6aad7ba5': '0xc15eed0fd21d0d1f9ed6c99036fd22b8330cd72d',
  '0x7ab4bc35088739f0f9af66c20d2345ff808523c708222be901270c69c52b2bee': '0xbdfaedcd5c26e2717d3a8a8328c9d08a9b33c8e9',
  '0xf4e386d53e73ed01a5ae5de90fae1231b627d07e34dd7a7ddc9bd69b545bb7b8': '0xf78e79d60ecd877d3287d7f2e075e72d940e18b6',
  '0x4985d82e099e4a7477279428d977db80ca91f0672c04c2dafb528485a4e53d31': '0xd812e2507ff2f7f699121c64ae803d34ecc5f851',
  '0x4e713ade7b5c1c93a215c3b82ff9b7dfe0ad1c8fd9c7a959cf613a3b32cc07c4': '0x3f3862db33d78195281e6ffacda76a8f0d4cd5f8',
  '0x1180b2b24421b1416204d23457069e3e576f903dafeabe18639f250049237356': '0xdc9e2578e16a3db06b2bd202fa814d284cbffc57',
  '0xa1f96dc7aea58d9ec178f659261a8e630e23625d4084f430f1a3dc1e1b177ca0': '0x6d429368e77044f84b5f48475824a429e0d607f1',
  '0xd3d8c4ba5dd9a0ece33a822bb4a44c2516be4044996dbd79281cc0b8a8eed2c2': '0x4f326995bf84c72d496675028aab4ee53769cd5d',
  '0xd9f9b01592f6041a459cc64cc583179fb3068b9521042dcba351a8a858a4d853': '0x70b868ccf1fc76376a350e5037450098eee729dc',
  '0x86988931274117b9d9576942ddd0c6aa17e6ce2f99f6156b640157dc942915cc': '0x70b868ccf1fc76376a350e5037450098eee729dc',
  '0x2a581a734bc4eca204f4cf14033a8e0eb27c6b60792867534646cd0c6fb21ace': '0x70b868ccf1fc76376a350e5037450098eee729dc',
  '0x211f3fddbb02f07feeae2dfc0f6a9c1536e14222cddcbd547941077775b0f019': '0x7d67fb7285206910874c5eee05a8273a6acce506',
  '0xc5a5846958a597103ab2a7cc3944423fbb56826d80c79dba99a635338c6a9755': '0xcd5dc0d8b0376e839718ca4dbca895706d66b122',
  '0x4b95f11ac20e81766ecaa040671c12817c5addf40a918e2f47a0c20b6b21563e': '0x7eb5a7bfa2b3b73cebec5df14228c328aff3dd0d',
  '0x53390bd5c1dbe9db5d17ee6b9c81ce86c2e1f8670800d50e0654a622755804ce': '0x7eb5a7bfa2b3b73cebec5df14228c328aff3dd0d',
  '0x93680ea340113bf5a62c2da6786931f50a3e5dce64918bee991715da25a30fa0': '0xf56afabd18fd6215fdb308f953681fb408a4e32d',
  '0xf85dae1ad9e15664475a56a5148fc01d485615cbb5655a3f256e59a44b512466': '0xf56afabd18fd6215fdb308f953681fb408a4e32d',
  '0x1d80e6946ab19af4812f88df22f30f7a1be8e2adabd94b89bcd08edc5a042c20': '0x0fa3f98072e08f915c4bfe3e33b130dfc1766bdc',
  '0xa8533b6ab69ea386b35fe743a12ec4e6abaa5bcbf99a2df074b4cd5c57e77a33': '0x0fa3f98072e08f915c4bfe3e33b130dfc1766bdc',
  '0xb905cdaa83829fc60742864ca6371ee5b73ebd44a9568f430225d0df55d4391d': '0x0fa3f98072e08f915c4bfe3e33b130dfc1766bdc',
  '0x0ae56d658bd79fa2a7fb849308ac87f63cec39e893fd75a90e7e360f3de4a7ac': '0x685c3e7f7ba7ef1d888df1b0bd1ca36b6b4e67a3',
});

const RIPPLE_TX_ADDR_REMAP = validateRemap(Rules.HASH_256_UC, Rules.HASH_160_0X_LC, {
  'D16AA0622C9597BD204C9D8C7FFED8348886A1525876FB0210AD420BF0B35E2D': '0x98df45b4db3abb978f0cc10a5cc3d822d23c2a7c',
});

const UTXO_TX_ADDR_REMAP = validateRemap(Rules.HASH_256_LC, Rules.HASH_160_0X_LC, {
  // cardano
  '63f3713e9558622adfb4b41ac268fda850a83b3065c4dee18698c9811d372491': '0xcbe0f71d5edecd9a4440f878ccd64040f490f9d9',
  // dogecoin
  'b0ea4ecd35672f528bee4b62475be522c0ff94a85550500c37aef9287f59a2d5': '0x4c1194425e40385ee68e71e2b4d141277beeec10',
  'e6f6ba143e1149be74bbaee6b71dcf59c91276a4b75102eb14517f140cfbaa7b': '0x4c1194425e40385ee68e71e2b4d141277beeec10',
});

export function importRawCsv(rawCsvPath: string): Sacrifice[] {
  const text = fs.readFileSync(rawCsvPath, 'utf-8');
  const rows = text.split(/\r\n|\r|\n/);

  validateHeader(rawCsvPath, rows.shift());

  const sacrifices = rows.map((row, rowInd) => importRawCsvRow(rawCsvPath, row, rowInd));

  applyInterventions(sacrifices);
  applyTransactionInterventions(sacrifices);

  return sacrifices;
}

export function importRawCsvRow(rawCsvPath: string, row: string, rowInd: number): Sacrifice {
  const f = row.split(',');

  try {
    validateFieldCount(f.length);

    const network = validateFieldType(f, 4, Networks);
    const source = validateFieldType(f, 9, Sources);
    const ticker = f[7];

    const s: SacrificeRow = {
      minedTimestamp: validateField(f, 0, Rules.ISO_DATETIME),
      transactionHash: validateField(f, 1, TX_HASH_RULE_MAP[network]),
      creditAddressId: validateField(f, 2, Rules.POSITIVE_INTEGER),
      isSens: validateField(f, 3, Rules.BOOLEAN),
      network,
      blockId: validateField(f, 5, Rules.NONZERO_INTEGER),
      currency: validateField(f, 6, Rules.CURRENCY),
      ticker,
      decimals: validateField(f, 8, Rules.POSITIVE_INTEGER),
      source,
      creditAddress: validateField(f, 10, Rules.HASH_160_0X_LC),
      advertisedFor: validateField(f, 11, Rules.BOOLEAN),
      ignore: validateField(f, 12, Rules.BOOLEAN),
      amount: validateField(f, 13, Rules.POSITIVE_INTEGER),
      usdPrice: validateField(f, 14, Rules.DECIMAL),
    };

    return {
      minedTimestamp: new Date(s.minedTimestamp),
      transactionHash: s.transactionHash,
      creditAddressId: +s.creditAddressId,
      isSens: s.isSens === 'true',
      network,
      blockId: +s.blockId,
      currency: s.currency,
      decimals: +s.decimals,
      source,
      creditAddress: s.creditAddress,
      advertisedFor: s.advertisedFor === 'true',
      ignore: s.ignore === 'true',
      amount: BigInt(s.amount),
      usdPrice: scaleToDecimals18(s.usdPrice),
      ticker: s.ticker,

      sourceRank: SOURCE_RANK_MAP[source],
      rawRow: row,
    };
  } catch (err) {
    if (err instanceof InvalidRowError) {
      err.addContext(rawCsvPath, row, rowInd);
    }
    throw err;
  }
}

function applyTransactionInterventions(sacrifices: Sacrifice[]) {
  const entries = sacrifices.map<[string, number]>(({
    creditAddress,
    creditAddressId,
  }) => ([creditAddress, creditAddressId]));
  const creditAddressToId = new Map(entries);
  let maxCreditAddressId = sacrifices.reduce((max, s) => (
    Math.max(max, s.creditAddressId)
  ), 0);
  for (const s of sacrifices) {
    const attributeTo = TX_ADDR_REMAP.get(s.transactionHash)
      || RIPPLE_TX_ADDR_REMAP.get(s.transactionHash)
      || UTXO_TX_ADDR_REMAP.get(s.transactionHash);
    if (!attributeTo) {
      continue;
    }
    const log = logIntervention(s);
    let creditAddressId = creditAddressToId.get(attributeTo);
    if (!creditAddressId) {
      creditAddressId = maxCreditAddressId;
      maxCreditAddressId += 1;
      creditAddressToId.set(attributeTo, creditAddressId);
    }
    s.creditAddress = attributeTo;
    s.creditAddressId = creditAddressId;
    log(s);
  }
}

function logIntervention(before: Sacrifice) {
  const {
    transactionHash,
    creditAddress: fromAddr,
    creditAddressId: fromAddrId,
  } = before;
  return (after: Sacrifice) => {
    const {
      creditAddress: toAddr,
      creditAddressId: toAddrId,
    } = after;
    console.log(`INTERVENTION: ${transactionHash.startsWith('0x') ? '' : '  '}${transactionHash}: ${fromAddr.startsWith('0x') ? '' : '0x'}${fromAddr} (${fromAddrId}) -> ${toAddr} (${toAddrId})`);
  };
}

function applyInterventions(sacrifices: Sacrifice[]) {
  const toAddrSet = new Set(ADDR_REMAP.values());

  const toAddrIdMap = new Map<Address, number>();
  const updating = [] as Sacrifice[];

  for (const s of sacrifices) {
    const addr = s.creditAddress;

    if (ADDR_REMAP.has(addr)) {
      updating.push(s);
    } else if (toAddrSet.has(addr)) {
      toAddrIdMap.set(addr, s.creditAddressId);
    }
  }

  for (const s of updating) {
    const log = logIntervention(s);

    const toAddr = ADDR_REMAP.get(s.creditAddress) as string; // Exists due to above test
    const toAddrId = toAddrIdMap.get(toAddr) || s.creditAddressId;

    s.creditAddress = toAddr;
    s.creditAddressId = toAddrId;

    log(s);
  }
}

export function dedupSourcePrices(sacrifices: Sacrifice[]) {
  const couldAccept = new Map<string, Sacrifice>();
  const ignoringSet = new Set<string>();
  const notIgnoringSet = new Set<string>();
  const advertisedForSet = new Set<string>();
  const salesSet = new Set<string>();
  for (const s of sacrifices) {
    const key = sacrificeToKey(s);
    if (s.advertisedFor) {
      advertisedForSet.add(key);
    } else if (!s.ignore) {
      notIgnoringSet.add(key);
    } else {
      ignoringSet.add(key);
    }
    if (!s.sourceRank) {
      salesSet.add(key);
    }
    const existing = couldAccept.get(key);
    if (!existing || (s.sourceRank < existing.sourceRank && s.usdPrice > 0n)) {
      couldAccept.set(key, s);
    }
  }
  const advertisedFor = setToMap(couldAccept, advertisedForSet);
  const notIgnoring = setToMap(couldAccept, notIgnoringSet);
  const ignoring = setToMap(couldAccept, ignoringSet);
  const sales = setToMap(couldAccept, salesSet);
  const categories = new Map<string, Map<string, Sacrifice>>([
    ['advertisedFor', advertisedFor],
    ['notIgnoring', notIgnoring],
    ['ignoring', ignoring],
    ['sales', sales],
  ]);
  return {
    categories,
    couldAccept,
    advertisedFor,
    notIgnoring,
    ignoring,
    sales,
  };
}

const setToMap = (map: Map<string, Sacrifice>, set: Set<string>): Map<string, Sacrifice> => (
  new Map([...set.values()].map((key) => ([key, map.get(key) as Sacrifice])))
);

export const filterUndesirables = (sacrifices: Sacrifice[]): Sacrifice[] => (
  // only not ignored, and ignored sacrifices
  // with a source rank of "0" or "sale"
  // will be able to get through this filter
  sacrifices.filter((sacrifice) => (!sacrifice.ignore || !sacrifice.sourceRank))
);

function sacrificeToKey(s: Sacrifice): string {
  return `${s.transactionHash}-${s.network}-${s.currency}-${s.blockId}-${s.creditAddress}`;
}
