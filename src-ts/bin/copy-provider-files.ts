import * as fs from 'fs';
import * as os from 'os';
import * as path from 'path';
import { fromProject } from '../../src-ts/paths';

import yargs from 'yargs';

const argv = yargs.options({
  to: {
    type: 'string',
    require: true,
    description: 'the folder to copy the relevant provider files to',
  },
  template: {
    type: 'string',
    require: true,
    describe: 'the structure to place files into',
  },
  force: {
    type: 'boolean',
    default: false,
    describe: 'overwrite previously existing output',
  },
}).parseSync(process.argv.slice(2));

main().catch(console.error);

async function main() {
  const { to, template, force } = argv;
  await run('pulsechain', '.', to, template, force);
  await run('pulsex', 'pulsex', to, template, force);
}

async function run(project: string, filesIn: string, to: string, template: string, force: boolean) {
  const fileNames = fromProject(filesIn)
  const files = [
    fileNames.EXPORT_CREDITS_CSV_NAME,
    fileNames.EXPORT_POINT_EVENT_NAME,
    fileNames.EXPORT_RAW_REDUCED_NAME,
  ];
  const rootDir = path.join(__dirname, '..', '..');
  const dataDir = path.join(rootDir, 'data');
  const fileStreams = await Promise.all(files.map((file) => (
    fs.promises.readFile(path.join(dataDir, project, file))
  )));

  const templateResolved = template.replace('{project}', project);
  const toDir = to.replace('~', os.homedir());
  const finalDir = path.join(toDir, templateResolved);
  try {
    await fs.promises.access(finalDir, fs.constants.R_OK | fs.constants.W_OK);
    if (force) {
      await fs.promises.rm(finalDir, {
        recursive: true,
        force: true,
      })
    } else {
      console.log('unable to write to dir. already exists', finalDir);
      return;
    }
  } catch (err: unknown) {
    console.log('moving forward, writing', project);
  }
  await fs.promises.mkdir(finalDir, {
    recursive: true,
  });
  await Promise.all(fileStreams.map(async (file, index) => {
    const fileName = files[index];
    const outputPath = path.join(finalDir, fileName);
    const filePath = path.join(outputPath);
    console.log(filePath);
    await fs.promises.writeFile(filePath, file);
  }));
}
