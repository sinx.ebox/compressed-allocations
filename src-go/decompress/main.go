package main

import (
	"compressed-allocations/src-go/utils"
	"fmt"
	"math/big"
	"os"
)

func main() {
	binData, err := os.ReadFile(utils.CreditsBinPath)
	utils.PanicFail(err)

	fmt.Printf("Reading %d bytes from compressed file '%s'\n", len(binData), utils.CreditsBinPath)

	for pos := 0; pos < len(binData); {
		recordLen := int(binData[pos])
		if recordLen < utils.MinRecordLen || recordLen > utils.MaxRecordLen {
			utils.PanicFail(fmt.Errorf("bad record length '%d' at pos %d of file '%s'", recordLen, pos, utils.CreditsBinPath))
		}
		pos++

		creditPos := pos + utils.AddressBinLen
		addrBin := binData[pos:creditPos]

		pos += recordLen
		creditBin := binData[creditPos:pos]

		addrStr := utils.BinToHexAddr(addrBin)
		creditStr := utils.BinToHexCredit(creditBin)
		creditPls := utils.WeiToEth(new(big.Int).SetBytes(creditBin))

		fmt.Println("Addr:", addrStr, "| Credit:", creditStr, fmt.Sprintf("(%f PLS)", creditPls))
	}
}
