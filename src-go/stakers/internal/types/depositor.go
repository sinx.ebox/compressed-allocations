package types

import (
	"math/big"
	"strings"

	"github.com/ethereum/go-ethereum/common/hexutil"
)

type Depositor struct {
	Id                         string `json:"id"`
	TotalAmountDepositedInGwei string `json:"totalAmountDeposited"`
}

type DepositorById []Depositor

func (s DepositorById) Len() int {
	return len(s)
}
func (s DepositorById) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s DepositorById) Less(i, j int) bool {
	return s[i].Id < s[j].Id
}

func (d Depositor) ToCsvLine() []string {
	depositedGwei := new(big.Int)

	// nolint: gomnd
	depositedGwei, _ = depositedGwei.SetString(d.TotalAmountDepositedInGwei, 10)

	// nolint: gomnd
	depositedWei := new(big.Int).Mul(depositedGwei, big.NewInt(1e9))

	return []string{strings.ToLower(d.Id), hexutil.EncodeBig(depositedWei), depositedWei.String()}
}

func DepositorFromCsvLine(line []string) Depositor {
	// We store total deposit in WEI, so need to convert to GWEI again
	depositedWei := new(big.Int)

	// nolint: gomnd
	depositedWei, _ = depositedWei.SetString(line[2], 10)

	// nolint: gomnd
	depositedGwei := new(big.Int).Div(depositedWei, big.NewInt(1e9))

	return Depositor{
		Id:                         line[0],
		TotalAmountDepositedInGwei: depositedGwei.String(),
	}
}
