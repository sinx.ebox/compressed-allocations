package audit

import (
	"sort"
	"strings"

	"compressed-allocations/src-go/stakers/internal/csv_saver"
	"compressed-allocations/src-go/stakers/internal/types"

	log "github.com/sirupsen/logrus"

	"github.com/pkg/errors"
)

const (
	failedToReadFromEventCsvDestinationErr    = "failed to read from eventCsvDestination"
	failedToReadFromTheGraphCsvDestinationErr = "failed to read from theGraphCsvDestination"
	numberOfDepositorMustMatchErr             = "number of depositors must match in both csv"
	depositorAddressesDontMatchErr            = "depositors addresses don't match"
	totalDepositedAmountMismatchErr           = "total deposited amount mismatch"
)

func CompareCSV(eventCsvDestination, theGraphCsvDestination string) ([]types.Depositor, error) {
	fromEvents, err := csv_saver.LoadFromCSV(eventCsvDestination)
	if err != nil {
		return nil, errors.Wrap(err, failedToReadFromEventCsvDestinationErr)
	}

	fromTheGraph, err := csv_saver.LoadFromCSV(theGraphCsvDestination)
	if err != nil {
		return nil, errors.Wrap(err, failedToReadFromTheGraphCsvDestinationErr)
	}

	if len(fromEvents) != len(fromTheGraph) {
		log.Errorf("Audit failed. Number of depositors must match! (The Graph: %d, Events: %d)",
			len(fromTheGraph), len(fromEvents),
		)

		return nil, errors.New(numberOfDepositorMustMatchErr)
	}

	// Prepare for comparison
	sort.Sort(types.DepositorById(fromEvents))
	sort.Sort(types.DepositorById(fromTheGraph))

	for i, eventDepositor := range fromEvents {
		theGraphDepositor := fromTheGraph[i]

		if strings.Compare(eventDepositor.Id, theGraphDepositor.Id) != 0 {
			log.Errorf("Audit failed. Unexpected depositor address mismatch. (The Graph: %s, Events: %s)",
				theGraphDepositor.Id,
				eventDepositor.Id,
			)

			return nil, errors.New(depositorAddressesDontMatchErr)
		}

		if eventDepositor.TotalAmountDepositedInGwei != theGraphDepositor.TotalAmountDepositedInGwei {
			log.Errorf("Audit failed. Total amount deposited amount for address %s missmatch. (The Graph: %s, Events: %s)",
				theGraphDepositor.Id,
				theGraphDepositor.TotalAmountDepositedInGwei,
				eventDepositor.TotalAmountDepositedInGwei,
			)

			return nil, errors.New(totalDepositedAmountMismatchErr)
		}
	}

	return fromTheGraph, nil
}
