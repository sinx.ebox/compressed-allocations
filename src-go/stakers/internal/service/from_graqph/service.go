package from_graqph

import (
	"compressed-allocations/src-go/stakers/internal/config"
	"compressed-allocations/src-go/stakers/internal/service/from_graqph/grapqhl_client"
	"compressed-allocations/src-go/stakers/internal/types"
	"compressed-allocations/src-go/utils"
)

// Client represents GraphQL client.
type service struct {
	cfg    *config.Config
	client grapqhl_client.StakingDataRetriever
}

func NewClient(cfg *config.Config) *service {
	return &service{
		cfg:    cfg,
		client: grapqhl_client.NewClient(cfg.GraphAddress),
	}
}

func (svc *service) GetTotalDepositorsAmount() (int, error) {
	return svc.client.GetTotalDepositorsAmount()
}

func (svc *service) GetAllStakingData(totalDepositors int) ([]types.Depositor, error) {
	var tempResult []types.Depositor
	var finalResult []types.Depositor
	var err error

	bar := utils.InitProgressBar(totalDepositors, svc.cfg.GraphPageSize, "All staking data")

	lastReceivedID := ""
	for i := 0; ; i++ {
		err = bar.Add(1)
		if err != nil {
			return nil, err
		}

		tempResult, err = svc.client.GetStakingDataByBunch(svc.cfg.GraphPageSize, lastReceivedID)
		if err != nil {
			return nil, err
		}
		finalResult = append(finalResult, tempResult...)

		// if we hit the last bunch, it's length will be less than page size, so break the loop and return final result
		if len(tempResult) != svc.cfg.GraphPageSize {
			break
		}

		lastReceivedID = tempResult[svc.cfg.GraphPageSize-1].Id
	}

	return finalResult, nil
}
