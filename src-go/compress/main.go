package main

import (
	"bytes"
	"compressed-allocations/src-go/utils"
	"encoding/csv"
	"fmt"
	"os"
)

func main() {
	csvData, err := os.ReadFile(utils.CreditsCsvPath)
	utils.PanicFail(err)

	csvReader := csv.NewReader(bytes.NewReader(csvData))
	csvRows, err := csvReader.ReadAll()
	utils.PanicFail(err)

	binFile, err := os.Create(utils.CreditsBinPath)
	utils.PanicFail(err)
	defer func() {
		err := binFile.Close()
		utils.PanicFail(err)
	}()

	var recordBuf [1 + utils.MaxRecordLen]byte

	for rowInd, fields := range csvRows {
		addrStr := fields[0]
		creditStr := fields[1]

		addrBin, err := utils.HexToBinAddr(addrStr)
		if err != nil {
			utils.PanicFail(fmt.Errorf("bad address '%s' in row %d of file '%s'", addrStr, rowInd+1, utils.CreditsCsvPath))
		}

		creditBin, err := utils.HexToBinCredit(creditStr)
		if err != nil {
			utils.PanicFail(fmt.Errorf("bad credit value '%s' in row %d of file '%s'", creditStr, rowInd+1, utils.CreditsCsvPath))
		}

		recordLen := utils.AddressBinLen + len(creditBin)
		entry := recordBuf[:1+recordLen]
		entry[0] = byte(recordLen)
		record := entry[1:]

		copy(record, addrBin)
		copy(record[utils.AddressBinLen:], creditBin)

		_, err = binFile.Write(entry)
		utils.PanicFail(err)
	}

	fmt.Printf("Wrote %d rows to compressed file '%s'\n", len(csvRows), utils.CreditsBinPath)
}
