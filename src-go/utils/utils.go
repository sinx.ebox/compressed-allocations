package utils

import (
	"encoding/hex"
	"errors"
	"math"
	"math/big"
	"strings"
	"time"

	"github.com/schollz/progressbar/v3"
	log "github.com/sirupsen/logrus"
)

// nolint: gosec, stylecheck
const CreditsBinPath = "./data/credits.bin"

// nolint: gosec, stylecheck
const CreditsCsvPath = "./data/credits.csv"

const AddressBinLen = 20
const MaxCreditBinLen = 32
const MinRecordLen = AddressBinLen + 1               // Excludes the record length prefix byte
const MaxRecordLen = AddressBinLen + MaxCreditBinLen // Excludes the record length prefix byte

const WeiPerEth = 1e18

// nolint: gochecknoglobals
var weiPerEthFloat = big.NewFloat(WeiPerEth)

func PanicFail(e error) {
	if e != nil {
		panic(e)
	}
}

func HexToBinAddr(hexAddress string) ([]byte, error) {
	if len(hexAddress) != 2+2*AddressBinLen || !strings.HasPrefix(hexAddress, "0x") {
		return nil, errors.New("invalid")
	}

	return hex.DecodeString(hexAddress[2:])
}

func HexToBinCredit(hexCredit string) ([]byte, error) {
	n := len(hexCredit)
	if n <= 2 || n > 2+2*MaxCreditBinLen || !strings.HasPrefix(hexCredit, "0x") {
		return nil, errors.New("invalid")
	}
	hexCredit = hexCredit[2:]
	if n%2 == 1 {
		hexCredit = "0" + hexCredit
	}

	return hex.DecodeString(hexCredit)
}

func BinToHexAddr(bytes []byte) string {
	return "0x" + hex.EncodeToString(bytes)
}

func BinToHexCredit(bytes []byte) string {
	bytesAsString := hex.EncodeToString(bytes)
	if bytesAsString[0] == '0' {
		bytesAsString = bytesAsString[1:]
	}

	return "0x" + bytesAsString
}

func WeiToEth(wei *big.Int) *big.Float {
	weiFloat := new(big.Float).SetInt(wei)

	return new(big.Float).Quo(weiFloat, weiPerEthFloat)
}

func TimeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Infof("%s took %s", name, elapsed)
}

func InitProgressBar(numBlocks, batchSize int, description string) *progressbar.ProgressBar {
	numRequests := int64(math.Ceil(float64(numBlocks) / float64(batchSize)))

	return progressbar.Default(numRequests, description)
}
